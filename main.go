package main

import (
	"fmt"
	"github.com/silkeh/ipcipher"
	"net"
	"syscall/js"
)

var lastPass string
var key *ipcipher.Key

func getForm() (addr, pass js.Value) {
	document := js.Global().Get("document")
	addr = document.Call("getElementById", "addr")
	pass = document.Call("getElementById", "pass")
	return
}

func getButtons() (e, d, c js.Value) {
	document := js.Global().Get("document")
	e = document.Call("getElementById", "encrypt")
	d = document.Call("getElementById", "decrypt")
	c = document.Call("getElementById", "showPass")
	return
}

func getError() js.Value {
	return js.Global().Get("document").Call("getElementById", "error")
}

func setError(s string) {
	e := getError()

	fmt.Printf("Error: %s\n", s)

	e.Set("innerHTML", s)
	e.Set("style", "display: block")
}

func clearError(s string) {
	getError().Set("style", "display: none")
}

func togglePass(this js.Value, args []js.Value) interface{} {
	_, _, c := getButtons()
	_, p := getForm()

	if c.Get("checked").Bool() {
		p.Set("type", "text")
	} else {
		p.Set("type", "password")
	}

	return nil
}

func generateKey(pass js.Value) *ipcipher.Key {
	v := pass.Get("value").String()
	if key != nil && v == lastPass {
		return key
	}

	lastPass = v
	key = ipcipher.GenerateKeyFromPassword(v)

	return key
}

func run(f func(*ipcipher.Key, net.IP, net.IP) error) {
	addr, pass := getForm()

	// Run key derivation
	key := generateKey(pass)

	// Encrypt
	ip := net.ParseIP(addr.Get("value").String())
	err := f(key, ip, ip)
	if err != nil {
		setError(err.Error())
		return
	}

	// Set new IP address
	addr.Set("value", ip.String())
}

func encrypt(this js.Value, args []js.Value) interface{} {
	run(ipcipher.Encrypt)
	return nil
}

func decrypt(this js.Value, args []js.Value) interface{} {
	run(ipcipher.Decrypt)
	return nil
}

func main() {
	// Set encrypt/decrypt button onclick
	e, d, c := getButtons()
	e.Set("onclick", js.FuncOf(encrypt))
	d.Set("onclick", js.FuncOf(decrypt))
	c.Set("onclick", js.FuncOf(togglePass))

	<-make(chan struct{}, 0)
}
